$(function() {
    $('.closeInCart').click(
        function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            console.log($(this)[0].dataset.id);
            $.ajax({
                url: '/deleteshoppingcart',
                type: 'POST',
                data: {
                    id: $(this)[0].dataset.id
                },

            });

        }
    );

});