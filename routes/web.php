<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/users/{id}/courses/{course}', 'Homecontroller@signup');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('items', 'ItemController');

Route::get('/hole', 'PracticeController@hole');

//
//Route::get('/hikingweb', 'HikingController@index');
//Route::post('/hikingweb', 'HikingController@saveComment');

Route::post('/test', 'HikingController@test');

Route::get('/hikingweb/{pNo?}', 'HikingController@item');
Route::post('/hikingweb/', 'HikingController@saveComment');

Route::get('/shoppingcart/', 'HikingController@shoppingCart');
Route::post('/addshoppingcart/', 'HikingController@addShoppingCart');
Route::post('/deleteshoppingcart/', 'HikingController@deleteShoppingCart');

Route::get('/addorder', 'HikingController@addOrder');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/who', 'HomeController@who');
Route::get('/logout', 'HomeController@logout');
Route::get('/selflogin', 'HomeController@selflogin');

Route::get('/home', 'HomeController@index')->middleware('auth');

Route::group(['middleware' => 'auth.basic'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
});

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');
