<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class PracticeController extends BaseController
{
    //use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function hole()
    {
        return view('hole');
    }
}
