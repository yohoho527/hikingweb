<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('selflogin', 'logout');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function who(Request $request)
    {
        dd(Auth::user());
    }

    public function selflogin()
    {
        $user = User::findOrFail(2);
        Auth::login($user);
    }

    public function logout()
    {

        Auth::logout();

    }
}
