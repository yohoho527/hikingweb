<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Cartlist;
use App\Comment;
use App\Production;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Http\Request;

class HikingController extends Controller
{

    //
    public function index()
    {}
    public function item($pNo = "111sada0-PRBL")
    {

        $p = new Production;
        $c = new Comment;

        $first = $p->selectByNo($pNo);
        $first->pSize = explode(",", $first->pSize);
        $first->pAlbum = explode(",", $first->pAlbum);
        $first->pTag = explode(",", $first->pTag);

        //$comment = $c->getAll($pNo);
        $comment = $first->comments;
        $avgRating = $c->calRating($pNo);
        $commentCount = $comment->count();
        // $comment = $c->getAll('111sada0-PRBL');

        //$this->saveComment();
        return view('layouts.itemBasic', ['item' => $first, 'avgRating' => $avgRating, 'commentCount' => $commentCount]);
    }

    public function saveComment(Faker $faker, Request $request)
    {

        //資料表相關
        // dd($request->all());
        // Comment::create($request->all());
        $c = new Comment;

        $c->uName = $faker->name;
        //$c->uName = "長頸鹿";
        $c->pNo = request('pNo');

        $c->pName = request('pName');

        $c->uAvatar = $c->getRandomAvatar();
        $c->cRating = request('cRating');

        $c->cComment = request('cComment');
        $c->cTime = Carbon::now()->toDateString();
        $c->production_id = request('production_id');
        $c->save();

        // echo $c->uName;
        echo json_encode(array(
            'id' => $c->id,
            'uName' => $c->uName,
            'cTime' => $c->cTime,
            'uAvatar' => $c->uAvatar,
            'cRating' => $c->cRating,
            'cComment' => $c->cComment,
            'production_id' => $c->production_id,
            'avgRating' => $c->calRating(request('pNo')),
            'commentCount' => $c->getAll(request('pNo'))->count(),
        ));
        //轉址
        // return redirect('/hikingweb');

        // echo request('pName');
        // var_dump(request('pName'));
        // var_dump("Mont-bell 長袖快乾排汗衣 男 初級藍 Cool Parka 1114460");
        // echo $c->getAll($c->pNo)->get(0)->cComment;
        // echo $c->getAll($c->pNo)->get(1)->cComment;
    }

    public function test(Request $request)
    {
        //$p = new Production;
        //$production = $p->selectByNo("111sada0-PRBL");

        // $production = Production::find(2);
        // dd($production->comments);
        // return $production;

        //多連多測試
        // $cart = Production::first();
        // // return $cart;
        // dd($cart->orders);

        // $production = Cart::get()->get(0)->productions;
        // dd($production);

        $test = Production::first()->pluck('pName', 'pPlace');
        // $validation = Validator::make($request->all(), ['text' => 'required| max:5']);
        // if ($validation->fails()) {
        //     dd('標單填寫錯誤');
        // }
        //var_dump($request->file('file1')->isValid());
        //dd($request->file('file1')->hasFile());
        return redirect('/shoppingcart');
    }

    public function searchProduction(string $keyword)
    {
        //搜尋產品
        $keyword = "優惠";
        $production = Production::where("pName", 'like', "%$keyword%")->orWhere("pTag", 'like', "%$keyword%")->orWhere("pNo", 'like', "%$keyword%")->orWhere("pSex", 'like', "%$keyword%")->orWhere("pType", 'like', "%$keyword%")->orWhere("pBrand", 'like', "%$keyword%")->orWhere("pPrice", 'like', "%$keyword%")->orWhere("pIntro", 'like', "%$keyword%")->orWhere("pPlace", 'like', "%$keyword%")->orWhere("pMaterial", 'like', "%$keyword%")->orWhere("pCupon_title", 'like', "%$keyword%")->get();
        dd($production->pName);
        return $production;
    }

    public function addShoppingCart()
    {

        if (Cart::where('user_id', 1)->first()) {
            $cart = Cart::where('user_id', 1)->first();
        } else {
            $cart = new Cart();
            $cart->ctNo = 123;
            $cart->ctState = true;
            $cart->user_id = 1;
        }

        $cart->ctNo = request('ctNo');

        $cartlist = new Cartlist();
        $cartlist->pNo = request('pNo');
        $production = Production::where('pNo', $cartlist->pNo)->first();
        $cartlist->pNum = request('pNum');
        $cartlist->pSize = request('pSize');

        $cart->cartlists()->save($cartlist);
        $cartlist->Productions()->save($production);

        $cartlist->save();
        $cart->save();

        return redirect('/shoppingcart');

    }

    public function deleteShoppingCart( /*Request $request*/)
    {
        $id = request('id');
        //$id = $request->input('id');
        $cartlists = Cartlist::find($id);
        //dd($cartlists);
        $production = $cartlists->productions[0];

        $cartlists->productions()->detach();
        $cartlists->delete();
        $production->delete();

    }

    public function shoppingCart()
    {
        $carts = Cart::where("user_id", '1')->first();
        $cartlists = $carts->cartlists;

        return view('layouts.shoppingCart', ['cartlists' => $cartlists]);

    }

    public function addOrder()
    {
        $cart_1 = Cart::findOrFail(1);
        $item_1 = \App\Production::findOrFail(1);
        $item_2 = \App\Production::findOrFail(2);

        $cart_1->productions()->save($item_1);
        $cart_1->productions()->save($item_2);
    }
}
