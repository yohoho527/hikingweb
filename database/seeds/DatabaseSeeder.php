<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $this->call(ProductionTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        $this->call(CartlistTableSeeder::class);
        $this->call(Cartlist_ProductionTableSeeder::class);
        $this->call(CartsSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
