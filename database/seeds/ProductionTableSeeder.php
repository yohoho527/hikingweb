<?php

use App\Production;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class ProductionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        //

        Production::truncate();
        // for ($i = 0; $i < 5; $i++) {
        //     # code...
        //     $production = factory(Production::class)->create();
        // }
        $production = factory(Production::class)->create();

        //第二個產品
        $image2 = "https://diz36nn4q02zr.cloudfront.net/webapi/imagesV3/Original/SalePage/5745320/0/123120?v=1";

        $album2 = "https://diz36nn4q02zr.cloudfront.net/webapi/imagesV3/Original/SalePage/5745320/0/123120?v=1,
        https://diz36nn4q02zr.cloudfront.net/webapi/imagesV3/Original/SalePage/5745320/1/123120?v=1,
        https://diz36nn4q02zr.cloudfront.net/webapi/imagesV3/Original/SalePage/5745320/2/123120?v=1";

        $intro2 = "商品編號<br>
        5745320<br>
        銷售重點<br>
        * Omni-Tech科技防水，同時兼具透氣舒適<br>
        * Omni-HEAT™ 鋁點保暖科技<br>
        * 450 fill power鵝絨<br>
        * 可調整腰圍<br>
        * 內附安全暗袋<br>
        * 可拆式人造皮草<br>
        * 下擺可調整式拉繩<br>
        * 前短後長版型<br>
        * 100% nylon<br>";

        $production2 = Production::create([
            'pNo' => '111sada0-PRBL', 'pName' => 'Columbia 哥倫比亞 女款- Omni-TECH™防水鋁點保暖羽絨大衣',
            'pPhoto' => $image2,
            'pAlbum' => $album2,
            'pDate' => \Carbon\Carbon::now(),
            'pBrand' => 'Mont-bell', 'pType' => '外套',
            'pSex' => '女', 'pTag' => '保暖,時尚,防水',
            'pRating' => 5,
            'pComntNum' => 1,
            'pComntRatingNum' => 5, 'pState' => '充足',
            'pPrice' => 3000, 'pSize' => 'XL,L,M,S', 'pIntro' => $intro2,
            'pPlace' => $faker->country,
            'pMaterial' => '100% Polyester', 'pWeight' => '181',
            'pComment' => $faker->realText(),
            'pCupon_title' => null,
            'pCupon_price' => null,
        ]);

        //第二個產品結尾
    }

}
