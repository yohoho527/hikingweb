@extends('layouts.allEmpty')
@section('content')
<script src="{{ asset('js/cart.js')}}"></script>
<script>
    $(function(){
        $(".closeMdInCart ").hide();
        $(".productionInCart").hover(function(){
           // $(this).children(".totalPrice").children(".closeMdInCart").fadeIn("fast");
            $(this).children(".totalPrice").children(".closeMdInCart").show();
        },
        function(){


           // $(this).children(".totalPrice").children(".closeMdInCart").fadeOut("fast");
            $(this).children(".totalPrice").children(".closeMdInCart").hide();

        });


    });
</script>
<div class="row">
    <div class="col ">
        {{ csrf_field() }}
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <h2 class="m-5">購物車清單</h2>
        <img src="" alt="">
        @for($i=0;$i<count($cartlists);$i++)


                <div class="row my-2 productionInCart">
                    <div class="col-12 col-md-2 d-flex justify-content-center">
                        <i class="fas fa-times-circle fa-lg h-0  d-md-none d-inline closeInCart" style="position: relative;left:90%;top:5%;width:0%;display: flex;" data-id={{ $cartlists->get($i)->id}}></i>
                        <img src="{{ $cartlists->get($i)->productions[0]->pPhoto}}" class="img-thumbnail">

                    </div>
                    <div class="col-4   col-md-2 align d-flex border  border-left-0 border-top-0 border-bottom-0 justify-content-center">
                        <h5 class=" align-self-center "> {{ $cartlists->get($i)->productions[0]->pName}}</h5>
                    </div>
                    <div class="col-2 align d-flex  justify-content-center ">
                        <h5 class=" align-self-center "> {{ $cartlists->get($i)->pSize}}</h5>
                    </div>
                    <div class="col-2 align d-flex  justify-content-center ">
                        <h5 class=" align-self-center ">$ {{ $cartlists->get($i)->productions[0]->pPrice}}</h5>
                    </div>

                    <div class="col-2 align d-flex  justify-content-center ">
                        <h5 class=" align-self-center ">ｘ {{ $cartlists->get($i)->pNum}}件</h5>
                    </div>

                    <div class="col-2 align d-flex  justify-content-center totalPrice">
                        <h5 class=" align-self-center ">$ {{$cartlists->get($i)->productions[0]->pPrice* $cartlists->get($i)->pNum}}</h5>
                        <i class="fas fa-times-circle fa-lg closeMdInCart closeInCart " style="position: relative;left:30%;top:5%"  data-id={{ $cartlists->get($i)->id}}></i>
                    </div>


                    <input type="hidden" value="{{  $cartlists->get($i)->id }}" name="id" >
                </div>


        @endfor

        @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div style="color:red">{{$error}}</div>
        @endforeach
    @endif
        {!! Form::open(['action'=>'HikingController@test','method'=>'POST','files'=>true]) !!}
        {!! Form::label("email", "顯示文字",['class'=>'awesome']) !!}

        {!! Form::textarea("text", "?", []) !!}

        {!! Form::checkbox("colors[]","yellow","checked", ['class'=>'options']) !!}
        {!! Form::checkbox("colors[]","blue","checked", ['class'=>'options']) !!}
        {!! Form::radio("colors2[]","yellow", "checked", []) !!}
        {!! Form::radio("colors2[]","blue", "checked", []) !!}

        {!! Form::file("file1", []) !!}


        {!! Form::submit("送出", []) !!}

        {!! Form::close() !!}
    </div>
</div>
@stop
