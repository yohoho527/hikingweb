@extends('layouts.allEmpty')
@section('content')


<!--內文-->
    <div class="container py-5">
        <div class="row">
          <div class="col">
              <ul class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">瀏覽商品</a></li>
                  <li class="breadcrumb-item"><a href="#">上衣</a></li>
                  <li class="breadcrumb-item active">外套</li>
              </ul>
          </div>
        </div>
        <div class="row">
          <!--商品圖片-->
          <div class="col-12 col-md-6">
            <!--大圖與箭頭-->
            <span class="bg-secondary cursor-pointer px-1 " style="position:absolute;top:35%;display: none;" id="album-left"><i class="fas fa-angle-left fa-2x text-white"></i></span>

            <a href="{{ $item->pPhoto }}" data-lightbox="image-1" data-title="產品照片" ><img src="{{ $item->pPhoto }}" class="w-100  img-thumbnail album-target"></a>

            <span class="bg-secondary cursor-pointer px-1"  style="position:absolute;top:35%;right:16px;" id="album-right"><i class="fas fa-angle-right fa-2x text-white"></i></span>

          <!--相簿小圖-->

            <div class="row py-2 px-3">
                {{--  @foreach ($item->pAlbum as $photo)
                <div class="col-3 cursor-pointer p-1"><img src="{{ $photo }}" class="w-100 img-thumbnail album-item" data-index="1"></div>
                @endforeach  --}}

                @for($i=1;$i<=count($item->pAlbum);$i++)
                <div class="col-3 cursor-pointer p-1"><img src="{{ $item->pAlbum[$i-1] }}" class="w-100 img-thumbnail album-item" data-index={{ $i }}></div>
                @endfor
              {{--  <div class="col-3 cursor-pointer p-1"><img src="https://awsimage1.banner.tw/store73/upload/2020Apr/png/20200402115035230_2.png" class="w-100 img-thumbnail album-item" data-index="1"></div>
              <div class="col-3 cursor-pointer p-1"><img src="https://awsimage1.banner.tw/store73/upload/2020Apr/png/20200402105322760_2.png" class="w-100 img-thumbnail album-item" data-index=2></div>  --}}

            </div>

          </div>
          <div class="col-12 col-md-6">
            <div class=" box-shadow rounded p-1 bg-primary text-white d-inline-block px-3">Mont-bell</div>
            <div class=" my-2 p-3">
              <h4>{{ $item->pName }}</h4>
            </div>
            <a href="" ><span class="badge badge-success  badge-pill ">{{ $item->pType }}</span></a>
            <a href="" ><span class="badge badge-primary  badge-pill ">{{ $item->pSex }}</span></a>

            @for($i=0;$i<count($item->pTag);$i++)

                <a href="" >
                @if($i%8==0)
                    <span class="badge badge-success  badge-pill ">
                @elseif($i%8==1)
                        <span class="badge badge-secondary  badge-pill ">
                @elseif($i%8==2)
                        <span class="badge badge-primary  badge-pill ">
                 @elseif($i%8==3)
                            <span class="badge badge-danger  badge-pill ">
                @elseif($i%8==4)
                            <span class="badge badge-warning  badge-pill ">
                @elseif($i%8==5)
                                <span class="badge badge-info  badge-pill ">
                @elseif($i%8==6)
                                    <span class="badge badge-light  badge-pill ">
                @elseif($i%8==7)
                                        <span class="badge badge-dark  badge-pill ">
                 @endif
                        {{   $item->pTag[$i]}}
                </span>
            </a>
            @endfor
            <hr>
            <div id="rating_area">
                @for($i=1;$i<=5;$i++)

                    @if($i<=$avgRating)

                        <img src="https://image.flaticon.com/icons/svg/1828/1828884.svg" style="width: 5%" id="star_result{{ $i }}">

                    @else

                        <img src="https://image.flaticon.com/icons/svg/1828/1828884.svg" style="width: 5%; opacity: 20%" id="star_result{{ $i }}">

                    @endif
                @endfor


              <p class="small pt-2">已有<b id="commentCount">{{ $commentCount }}</b>個人評價過此產品， 評分 : <b id="avgRating">{{  $avgRating}} ★</b></p>
              <hr>
            </div>
            <p class="my-2">商品編號:型號：1114460-PRBL</p><p class="float-right text-secondary"><i>上架日期:{{ $item->pDate }}</i></p>
            <p class="my-2">商品狀態：<span class="bg-success text-white rounded px-1">充足</span></p>

            <div class="my-3">
            <h2 class=" d-inline  mr-3">${{ $item->pPrice }} <h3 class="text-secondary d-inline "><strike>{{ $item->pCupon_price }}</strike><sup class="ml-3 text-danger ">{{ $item->pCupon_title }}</sup></h3>  </h2>
            </div>



            <form method="POST" action="addshoppingcart">
                {{ csrf_field() }}
              <h6 class="my-2 d-inline pr-5">尺寸:</h6>

              <select name="pSize">

                @for($i=0;$i<count($item->pSize);$i++)

                <option value="{{ $item->pSize[$i]}}">  {{ $item->pSize[$i]}}</option>
             @endfor
              </select>

              <h6>數量</h6>
                <input type="number" value="1" min="1" max="100" style="text-align: center;" name="pNum" class="form-control w-50" id=num>
                <input type="hidden" value=123 name="ctNo">
              <input type="hidden" value={{$item->pNo}} name="pNo">

              <h4>
                <input type="submit" class="btn-danger my-3 p-2" value="加入購物車">
              </h4>
            </form>
          </div>

        </div>





      <!--Tab panel -->


        <ul class="nav nav-tabs my-3">
          <li class="nav-item"><a data-toggle="tab" href="#home" class="nav-link active"><p><big>商品介紹</big></p></a></li>
          <li class="nav-item"><a data-toggle="tab" href="#menu1" class="nav-link"><p><big>評論區</big></p></a></li>
          <li class="nav-item"><a data-toggle="tab" href="#menu2" class="nav-link"><p><big>購買須知</big></p></a></li>
        </ul>

        <div class="tab-content ">
          <div id="home" class="tab-pane  active" >

            <div class="tab-div m-3 p-3">
              <p>{!! $item->pIntro !!}</p></p>
            </div>

          <h6 class="p-2 px-3 bg-success rounded text-white d-inline-block ml-3">產品規格</h6>
          <table class="table table-striped" >
            <tr>
              <th><strong>產地</strong></th>
              <td>{{ $item->pPlace }}</td>
            </tr>
            <tr>
              <th><strong>材質</strong></th>
              <td>{{ $item->pMaterial }}</td>
            </tr>
            <tr>
              <th><strong>重量</strong></th>
              <td>{{ $item->pWeight }} g</td>
            </tr>
          </table>

          </div>



          <!--留言評論區-->
          <div id="menu1" class="tab-pane fade  m-3 p-3">



        {{--  <form method="post" action="/hikingweb" enctype="multipart/form-data">  --}}
          {{ csrf_field() }}
          <meta name="csrf-token" content="{{ csrf_token() }}">
              <div id="rating_area" class="py-4">
                <img src="https://image.flaticon.com/icons/svg/1828/1828884.svg" style="width: 5%" id="star1">
                <img src="https://image.flaticon.com/icons/svg/1828/1828884.svg" style="width: 5%" id="star2">
                <img src="https://image.flaticon.com/icons/svg/1828/1828884.svg" style="width: 5%" id="star3">
                <img src="https://image.flaticon.com/icons/svg/1828/1828884.svg" style="width: 5%" id="star4">
                <img src="https://image.flaticon.com/icons/svg/1828/1828884.svg" style="width: 5%" id="star5">
              </div>

              <input type="hidden" name="cRating" ID="cRating"  value=5>
              <input type="hidden" name="pNo" value={{  $item->pNo }} id="pNo">
              <input type="hidden" name="pName" value="{{  $item->pName }}" id="pName">
              <input type="hidden" name="production_id" value={{ $item->id }}  id="production_id">
                <textarea class="form-control comment pb-cmnt-textarea" rows="5" name="cComment" id="commentText"></textarea>
                <input type="submit" name="" value="留言" class=" my-3" id="commentBtn">
              {{--  </form>  --}}

                {{--  {{ $item->comments[0]->uName }}  --}}


            <div id="commentBoard">
                @if(count($item->comments)!=0)
@foreach ( $item->comments as $c)

                {{--  {{ count($c)}}  --}}
                <div class="comment row"  id="comment{{ $c->id }}"  @if($loop->index>5)style="display:none" @endif>
                    <div class="col-12  ">

                              <div id="rating_area{{ $c->id }}" class="d-flex align-items-end">
                                <div class="rounded d-flex" style="overflow:hidden; width: 40px;height:40px"><img src="{{ $c->uAvatar }}" alt="" style="" class="w-100"></div>
                               <span class="px-2  align-self-end "  style="line-height:10px">


                                   <h5 class="m-0 d-flex"> {{ $c->uName }}</h5>
                                    <i><small >{{ $c->cTime }}</small></i>
                                </span>
                                @for($i=1;$i<=5;$i++)
                                    @if($i<=$c->cRating)
                                    <img src="https://image.flaticon.com/icons/svg/1828/1828884.svg" style="width: 2%">
                                    @else
                                    <img src="https://image.flaticon.com/icons/svg/1828/1828884.svg" style="width: 2%;opacity: 0.2">
                                    @endif
                                @endfor

                              </div>

                    </div>
                    <div class="col-12">
                        <p class="pt-3">{{ $c->cComment}}


                        </p>
                         <hr>

                    </div>
            </div>




    @endforeach
@endif
<button class="btn-danger" id="showComment">顯示全部評論</button>

            </div>
        </div>
          <div id="menu2" class="tab-pane fade  m-3 p-3">
          <article>
            <p class="text-danger">※ 照片內商品因拍攝條件及螢幕不同而略有色差，請以實際出貨商品為準。</p>
            <h6>商品尺寸說明</h6>
            <p>各品牌衣褲鞋帽產品的尺寸版型都有差異，個人穿著習慣亦會影響尺寸選擇，若您不確定適合的尺寸，歡迎於商品詢問欄提問， 我們將盡快依據原廠尺寸表及同仁穿著經驗回覆建議，但僅提供您作選購參考，恕無法保證所建議的尺寸100%符合您所需喔。</p>
            <ul >
              <li id="female-size-click" style="cursor: pointer"><strong>女性尺寸參考</strong></li>
              <div id="female-size" class="p-3">
               <article>
                  <p class="text-danger">※每個人的體脂與身材比例皆不同，以下對照表僅供參考，建議至門市試穿最準確，謝謝！</p>
                  <h3>女-上半身</h3>
                  ※理想體重－以"前者身高"為基準:理想體重為「偏瘦~正常」，後者則再加5kg
                        <table class="table table-striped ">
                          <tbody>
                            <tr class="table-dark thead-dark" >
                              <th>
                                身高</th>
                              <th>
                                胸圍(cm/英吋)</th>
                              <th>
                                腰圍(cm/英吋)</th>
                              <th>
                                理想體重</th>
                              <th>
                                亞洲尺寸</th>
                            </tr>
                            <tr>
                              <td>
                                152-158</td>
                              <td>
                                76~84/29~33</td>
                              <td>
                                57~65/22~25</td>
                              <td>
                                42~47kg</td>
                              <td>
                                XS~S</td>
                            </tr>
                            <tr>
                              <td>
                                158~163</td>
                              <td>
                                79~87/31~34</td>
                              <td>
                                60~68/23~26</td>
                              <td>
                                45.5~50kg</td>
                              <td>
                                S</td>
                            </tr>
                            <tr>
                              <td>
                                162~168</td>
                              <td>
                                82~90/32~35</td>
                              <td>
                                63~71/24~27</td>
                              <td>
                                48~53kg</td>
                              <td>
                                S~M</td>
                            </tr>
                            <tr>
                              <td>
                                167~173</td>
                              <td>
                                85~93/33~36</td>
                              <td>
                                66~74/25~29</td>
                              <td>
                                50~55kg</td>
                              <td>
                                M</td>
                            </tr>
                            <tr>
                              <td>
                                172~178</td>
                              <td>
                                88~97/34.5~38</td>
                              <td>
                                69~77/27~31</td>
                              <td>
                                53~58kg</td>
                              <td>
                                M~L</td>
                            </tr>
                          </tbody>
                        </table class="table table-striped ">
                        <div>
                          <h4 style="margin-bottom: 0;">
                          <b>體型比例參考</b></h4>
                          <table class="table table-striped ">
                            <tbody>
                              <tr class="thead-dark">
                                <th >
                                  身高</th>
                                <th>
                                  體重</th>
                                <th>
                                  體型</th>
                              </tr>
                              <tr>
                                <td>
                                  152cm</td>
                                <td>
                                  44kg</td>
                                <td>
                                  正常</td>
                              </tr>
                              <tr>
                                <td>
                                  158cm</td>
                                <td>
                                  44kg</td>
                                <td>
                                  偏瘦</td>
                              </tr>
                              <tr>
                                <td>
                                  158cm</td>
                                <td>
                                  50kg</td>
                                <td>
                                  正常</td>
                              </tr>
                              <tr>
                                <td>
                                  160cm</td>
                                <td>
                                  50kg</td>
                                <td>
                                  正常偏瘦</td>
                              </tr>
                              <tr >
                                <td>
                                  身高</td>
                                <td>
                                  身高-110</td>
                                <td>
                                  正常偏瘦</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <h2>
                          女-下半身</h2>
                        <table class="table table-striped ">
                          <tbody>
                            <tr class="thead-dark">
                              <th>
                                英吋</th>
                              <th>
                                腰圍(cm)</th>
                              <th>
                                臀圍(cm)</th>
                              <th>
                                亞洲尺寸</th>
                            </tr>
                            <tr>
                              <td>
                                22</td>
                              <td>
                                56</td>
                              <td>
                                77</td>
                              <td>
                                XS</td>
                            </tr>
                            <tr>
                              <td>
                                23</td>
                              <td>
                                59</td>
                              <td>
                                79</td>
                              <td>
                                XS</td>
                            </tr>
                            <tr>
                              <td>
                                24</td>
                              <td>
                                61</td>
                              <td>
                                82</td>
                              <td>
                                XS</td>
                            </tr>
                            <tr>
                              <td>
                                25</td>
                              <td>
                                63</td>
                              <td>
                                84</td>
                              <td>
                                XS</td>
                            </tr>
                            <tr>
                              <td>
                                26</td>
                              <td>
                                64</td>
                              <td>
                                86</td>
                              <td>
                                S</td>
                            </tr>
                            <tr>
                              <td>
                                27</td>
                              <td>
                                66</td>
                              <td>
                                90</td>
                              <td>
                                S~M</td>
                            </tr>
                            <tr>
                              <td>
                                28</td>
                              <td>
                                67</td>
                              <td>
                                93</td>
                              <td>
                                M</td>
                            </tr>
                            <tr>
                              <td>
                                29</td>
                              <td>
                                70</td>
                              <td>
                                97</td>
                              <td>
                                M~L</td>
                            </tr>
                            <tr>
                              <td>
                                30</td>
                              <td>
                                73</td>
                              <td>
                                103</td>
                              <td>
                                L</td>
                            </tr>
                            <tr>
                              <td>
                                31</td>
                              <td>
                                77</td>
                              <td>
                                106</td>
                              <td>
                                L~XL</td>
                            </tr>
                            <tr>
                              <td>
                                32</td>
                              <td>
                                80</td>
                              <td>
                                108</td>
                              <td>
                                XL</td>
                            </tr>
                            <tr>
                              <td>
                                33</td>
                              <td>
                                83</td>
                              <td>
                                110</td>
                              <td>
                                XL~2XL</td>
                            </tr>
                            <tr>
                              <td>
                                34</td>
                              <td>
                                87</td>
                              <td>
                                112</td>
                              <td>
                                2XL</td>
                            </tr>
                          </tbody>
                        </table>
               </article>
              </div>

              <li id="male-size-click" style="cursor: pointer"><strong>男性尺寸參考</strong></li>
              <div id="male-size">
                <article>
                      <p class="text-danger">※每個人的體脂與身材比例皆不同，以下對照表僅供參考，建議至門市試穿最準確，謝謝！</p>
                      <div>
                        <div class="man" id="man">
                          <h2>
                            男-上半身</h2>
                          ※理想體重－以"前者身高"為基準:理想體重為「正常~正常偏壯」，後者則再加5kg<br />
                          ※由於男生健身者比例偏多，若以體重衡量準確度會較低，建議測量實際胸圍或腰圍尺寸
                          <table class="table table-striped">
                            <tbody>
                              <tr class="thead-dark">
                                <th>
                                  身高</th>
                                <th>
                                  胸圍(cm/英吋)</th>
                                <th>
                                  腰圍(cm/英吋)</th>
                                <th>
                                  理想體重</th>
                                <th>
                                  亞洲尺寸</th>
                              </tr>
                              <tr>
                                <td>
                                  162~168</td>
                                <td>
                                  84~92/33~36</td>
                                <td>
                                  70~78/27~30</td>
                                <td>
                                  58~66kg</td>
                                <td>
                                  S</td>
                              </tr>
                              <tr>
                                <td>
                                  167~173</td>
                                <td>
                                  88~96/34~38</td>
                                <td>
                                  74~82/29~32</td>
                                <td>
                                  64~70kg</td>
                                <td>
                                  S~M</td>
                              </tr>
                              <tr>
                                <td>
                                  172~178</td>
                                <td>
                                  92~100/36~39</td>
                                <td>
                                  78~86/30~34</td>
                                <td>
                                  70~78kg</td>
                                <td>
                                  M~L</td>
                              </tr>
                              <tr>
                                <td>
                                  177~183</td>
                                <td>
                                  96~104/38~41</td>
                                <td>
                                  82~90/32~35</td>
                                <td>
                                  78~85kg</td>
                                <td>
                                  L~XL</td>
                              </tr>
                              <tr>
                                <td>
                                  182~188</td>
                                <td>
                                  100~108/39~42.5</td>
                                <td>
                                  82~94/32~37</td>
                                <td>
                                  85~90kg</td>
                                <td>
                                  XL</td>
                              </tr>
                            </tbody>
                          </table>
                          <div>
                            <h4 style="margin-bottom: 0;">
                              <b>體型比例參考</b></h4>
                            <table  class="table table-striped">
                              <tbody>
                                <tr class="thead-dark">
                                  <th>
                                    身高</th>
                                  <th>
                                    體重</th>
                                  <th>
                                    體型</th>
                                </tr>
                                <tr>
                                  <td>
                                    175cm</td>
                                  <td>
                                    70kg</td>
                                  <td>
                                    正常</td>
                                </tr>
                                <tr>
                                  <td>
                                    182cm</td>
                                  <td>
                                    75kg</td>
                                  <td>
                                    正常偏瘦</td>
                                </tr>
                                <tr>
                                  <td>
                                    165cm</td>
                                  <td>
                                    55kg</td>
                                  <td>
                                    偏瘦</td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <h2>
                            男-下半身</h2>
                          <table  class="table table-striped">
                            <tbody>
                              <tr class="thead-dark">
                                <th>
                                  英吋</th>
                                <th>
                                  腰圍(cm)</th>
                                <th>
                                  臀圍(cm)</th>
                                <th>
                                  亞洲尺寸</th>
                              </tr>
                              <tr>
                                <td>
                                  28</td>
                                <td>
                                  70</td>
                                <td>
                                  99</td>
                                <td>
                                  XS</td>
                              </tr>
                              <tr>
                                <td>
                                  29</td>
                                <td>
                                  74</td>
                                <td>
                                  101</td>
                                <td>
                                  S</td>
                              </tr>
                              <tr>
                                <td>
                                  30</td>
                                <td>
                                  78</td>
                                <td>
                                  103</td>
                                <td>
                                  M</td>
                              </tr>
                              <tr>
                                <td>
                                  31</td>
                                <td>
                                  80</td>
                                <td>
                                  105</td>
                                <td>
                                  M</td>
                              </tr>
                              <tr>
                                <td>
                                  32</td>
                                <td>
                                  84</td>
                                <td>
                                  107</td>
                                <td>
                                  M</td>
                              </tr>
                              <tr>
                                <td>
                                  33</td>
                                <td>
                                  88</td>
                                <td>
                                  109</td>
                                <td>
                                  L</td>
                              </tr>
                              <tr>
                                <td>
                                  34</td>
                                <td>
                                  90</td>
                                <td>
                                  111</td>
                                <td>
                                  L</td>
                              </tr>
                              <tr>
                                <td>
                                  36</td>
                                <td>
                                  94</td>
                                <td>
                                  113</td>
                                <td>
                                  XL</td>
                              </tr>
                              <tr>
                                <td>
                                  38</td>
                                <td>
                                  96</td>
                                <td>
                                  116</td>
                                <td>
                                  2XL</td>
                              </tr>
                              <tr>
                                <td>
                                  40</td>
                                <td>
                                  100</td>
                                <td>
                                  119</td>
                                <td>
                                  3XL</td>
                              </tr>
                              <tr>
                                <td>
                                  42</td>
                                <td>
                                  104</td>
                                <td>
                                  121</td>
                                <td>
                                  3XL</td>
                              </tr>
                              <tr>
                                <td>
                                  44</td>
                                <td>
                                  106</td>
                                <td>
                                  1259</td>
                                <td>
                                  4XL</td>
                              </tr>
                            </tbody>
                          </table>
                </article>
              </div>
              <li id="shoose-size-click" style="cursor: pointer"><strong>鞋子尺寸參考</strong></li>
              <div id="shoose-size">
                <article>

                          <article class="editor"><p>
                            <font style="color: red;">※每個品牌的尺寸皆有些微誤差，以下對照表僅供參考，建議參考原廠品牌的尺寸表或至門市試穿最準確，謝謝！</font></p>
                          <div>
                            <div class="shoes" id="shoes">
                              <h2>
                                女鞋尺寸換算表</h2>
                              <table  class="table table-striped">
                                <tbody>
                                  <tr class="thead-dark">
                                    <th>
                                      公分</th>
                                    <th>
                                      USA(美國)</th>
                                    <th>
                                      UK(英國)</th>
                                    <th>
                                      EU(歐洲)</th>
                                  </tr>
                                  <tr>
                                    <td>
                                      22</td>
                                    <td>
                                      5</td>
                                    <td>
                                      3</td>
                                    <td>
                                      34</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      22.5</td>
                                    <td>
                                      5.5</td>
                                    <td>
                                      3.5</td>
                                    <td>
                                      35</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      23</td>
                                    <td>
                                      6</td>
                                    <td>
                                      4</td>
                                    <td>
                                      36</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      23.5</td>
                                    <td>
                                      6.5</td>
                                    <td>
                                      4.5</td>
                                    <td>
                                      37</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      24</td>
                                    <td>
                                      7</td>
                                    <td>
                                      5</td>
                                    <td>
                                      38</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      24.5</td>
                                    <td>
                                      7.5</td>
                                    <td>
                                      5.5</td>
                                    <td>
                                      39</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      25</td>
                                    <td>
                                      8</td>
                                    <td>
                                      6</td>
                                    <td>
                                      40</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      25.5</td>
                                    <td>
                                      8.5</td>
                                    <td>
                                      6.5</td>
                                    <td>
                                      41</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      26</td>
                                    <td>
                                      9</td>
                                    <td>
                                      7</td>
                                    <td>
                                      41</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      26.5</td>
                                    <td>
                                      9.5</td>
                                    <td>
                                      7.5</td>
                                    <td>
                                      42</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      27</td>
                                    <td>
                                      10</td>
                                    <td>
                                      8</td>
                                    <td>
                                      43</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      27.5</td>
                                    <td>
                                      10.5</td>
                                    <td>
                                      8.5</td>
                                    <td>
                                      44</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      28</td>
                                    <td>
                                      11</td>
                                    <td>
                                      9</td>
                                    <td>
                                      44</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      28.5</td>
                                    <td>
                                      11.5</td>
                                    <td>
                                      9.5</td>
                                    <td>
                                      45</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      29</td>
                                    <td>
                                      12</td>
                                    <td>
                                      10</td>
                                    <td>
                                      45</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      29.5</td>
                                    <td>
                                      12.5</td>
                                    <td>
                                      10.5</td>
                                    <td>
                                      46</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      30</td>
                                    <td>
                                      13</td>
                                    <td>
                                      11</td>
                                    <td>
                                      46</td>
                                  </tr>
                                </tbody>
                              </table>
                              <h2>
                                男鞋尺寸換算表</h2>
                              <table  class="table table-striped">
                                <tbody>
                                  <tr class="thead-dark">
                                    <th>
                                      公分</th>
                                    <th>
                                      USA(美國)</th>
                                    <th>
                                      UK(英國)</th>
                                    <th>
                                      EU(歐洲)</th>
                                  </tr>
                                  <tr>
                                    <td>
                                      22</td>
                                    <td>
                                      4</td>
                                    <td>
                                      3.5</td>
                                    <td>
                                      34</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      22.5</td>
                                    <td>
                                      4.5</td>
                                    <td>
                                      4</td>
                                    <td>
                                      35</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      23</td>
                                    <td>
                                      5</td>
                                    <td>
                                      4.5</td>
                                    <td>
                                      36</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      23.5</td>
                                    <td>
                                      5.5</td>
                                    <td>
                                      5</td>
                                    <td>
                                      37</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      24</td>
                                    <td>
                                      6</td>
                                    <td>
                                      5.5</td>
                                    <td>
                                      38</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      24.5</td>
                                    <td>
                                      6.5</td>
                                    <td>
                                      6</td>
                                    <td>
                                      39</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      25</td>
                                    <td>
                                      7</td>
                                    <td>
                                      6.5</td>
                                    <td>
                                      40</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      25.5</td>
                                    <td>
                                      7.5</td>
                                    <td>
                                      7</td>
                                    <td>
                                      41</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      26</td>
                                    <td>
                                      8</td>
                                    <td>
                                      7.5</td>
                                    <td>
                                      41</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      26.5</td>
                                    <td>
                                      8.5</td>
                                    <td>
                                      8</td>
                                    <td>
                                      42</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      27</td>
                                    <td>
                                      9</td>
                                    <td>
                                      8.5</td>
                                    <td>
                                      43</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      27.5</td>
                                    <td>
                                      9.5</td>
                                    <td>
                                      9</td>
                                    <td>
                                      44</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      28</td>
                                    <td>
                                      10</td>
                                    <td>
                                      9.5</td>
                                    <td>
                                      44</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      28.5</td>
                                    <td>
                                      10.5</td>
                                    <td>
                                      10</td>
                                    <td>
                                      45</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      29</td>
                                    <td>
                                      11</td>
                                    <td>
                                      10.5</td>
                                    <td>
                                      45</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      29.5</td>
                                    <td>
                                      11.5</td>
                                    <td>
                                      11</td>
                                    <td>
                                      46</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      30</td>
                                    <td>
                                      12</td>
                                    <td>
                                      11.5</td>
                                    <td>
                                      46</td>
                                  </tr>
                                </tbody>
                              </table>
                </article>
              </div>
            </ul>

            <h6>運送及保固說明</h6>
            <p>商品以黑貓宅急便寄送，出貨時間為週一至週六。大型商品(如：帳篷、桌椅等)以貨運公司配送。
              台灣本島地區，下午 四點完成訂單，隔日送達。(偏遠山區、離島例外)。</p>
              <p>
                Schlagen 登山網所售出貨品均一年保固免費維修 。
              </p>

              <h6>運費說明</h6>
              <p>每次購物滿 2000 元以上免運費，未達 2000 元 需加付 120 元運費。</p>

              <h6>商品退換貨說明</h6>
              <p class="text-danger">安全吊帶、冰爪、繩索、勾環、確保器、滑輪等技術性攀登產品、食品、瓦斯燃料、以及貼身衛生商品如內褲、襪子、護具等，為保障貴顧客權益，售出後除瑕疵外，概不接受退換，麻煩請確認後再訂購，謝謝。 </p>
              <p>網路購物的消費者，都可以依照消費者保護法的規定，享有商品貨到日起七天猶豫期的權益。但猶豫期並非試用期，所以，您所退回的商品必須是全新的狀態、而且完整包裝；請注意保持商品本體、 配件、贈品、保證書、原廠包裝及所有附隨文件或資料的完整性，切勿缺漏任何配件或損毀原廠外盒。</p>

                <a href=""><p>如何申請退換貨</p></a>

                <h6>購物流程</h6>
                <p>STEP 1：將您想買的商品，放入購物車，並進入結帳程序。  </p>
                <p>STEP 2：選擇運送方式以及送貨地址。  </p>
                <p>STEP 3：選擇付費方式並完成結帳程序。  </p>
                <p>STEP 4：確認款項後出貨。</p>
                <p></p>
                <p>若有任何購物流程問題，歡迎加入 百岳官方LINE客服，將有專人為您服務。</p>

                <h6>付款方式</h6>
                <ul>
                  <li>服務範圍
                      <p>我們可以寄送到 EMS(International Express Mail Service) 國際快遞涵蓋的任何國家與地區。</p>
                  </li>
                  <li>無法寄送的商品
                    <p>
                      由於可能的尺寸誤差，100mountain暫不提供服飾類、鞋襪類等牽涉到使用者尺寸的商品之海外訂單服務，若會員顧客有特別需求者（如已購買過相同商品並確認尺寸無誤），可另外洽詢 E-mail: service@100mountain.com。任何航空出口禁制商品（如瓦斯瓶、寄達國禁制商品）也在非服務之列。<br>
      在訂購商品時，訂貨人本人務必事先清楚瞭解顧客所在國家、地區對有關進口之法律、規定等以及禁止進口的商品。萬一在送往國家、地區發生訂購商品被沒收情況時，本公司恕不承擔責任。本公司也無法因這些原因而將商品貨款和送貨手續費等退還給顧客。
                    </p>
                  </li>
                  <li>如何付費？
                  <p>商品以及運費，我們都採用信用卡線上刷卡付費。</p>
                  </li>
                  <li>運費計算
                  <p>寄送到香港、澳門、中國等地：請依順豐速運報價為收費參考。在確認您欲購買的商品可正常出貨之後，我們會估算實際運費並以電子郵件向您報價。<br>
      其它國家：請依台灣郵政EMS報價為收費參考。在確認您欲購買的商品可正常出貨之後，我們會估算實際運費並以電子郵件向您報價。</p>
                  </li>
                  <li>寄送須知
                  <p>因顧客輸入錯誤地址以致商品無法送抵時，本公司恕不承擔責任。此外，若顧客要求將商品再次寄送至正確地址時，本公司也將向顧客收取送貨郵資等費用。訂單經確認受理後，訂購內容(尺碼，顏色，數量等)、送貨地址等將無法變更，訂單亦無法取消。</p>
                  </li>
                  <li>額外費用
                  <p>除了網站上標明的價格以及向您收取的運費之外，不需要負擔任何額外的費用，但必須注意的是，商品寄達之後可能產生的當地政府關稅、規費、手續費等一切費用，由購買人（收件人）全部負擔。並透過送往地之投遞郵局或者送貨公司直接向顧客徵收。</p>
                  </li>
                  <li>寄送時程
                  <p>
                    從發貨至送達您手上，港澳地區約3天內送達，中國大陸各大城市約5天內送達，其餘國家依地區遠近而定，約7-21天送達（參考EMS寄送時程表）。以上送達時間為預估值，供您參考，若遇海關清關作業、航班調度或天候等其他不可抗力之因素影響可能會有所延遲，敬請見諒！
                  </p>
                  </li>
                </ul>

                <h6>關於退貨</h6>
                <ul>
                  <li>商品不是自己喜歡的
                  <p>請在收到商品3天內來信告知退貨原因（逾期恕不受理），並於兩週內將商品寄回到100mountain登山網（運費需由您自行負擔），在確認商品包裝及外觀無誤之後，我們會將款項在扣除我們的寄送運費之後退回您的信用卡帳戶（以台幣退款）。</p>
                  </li>

                  <li>
                    商品故障瑕疵
                  <p>
                    請在收到商品3天內（逾期恕不受理）將瑕疵故障位置拍照（至少三張清晰照片）寄到 E-mail:service@100mountain.com。在收到退貨申請及照片後我們會主動聯繫您作後續處理作業。
                  </p>
                    </li>
                </ul>

                <h6>特別注意事項</h6>
                <ul>
                  <li>100mountain保留隨時更改及解釋相關約定條款之權利。</li>
                  <li>100mountain保留接受訂單與否的權利。</li>
                  <li>本公司恕不發行訂購商品之原產國證明、出口簽證等出口證明。</li>
                  <li>顧客經仔細閱讀上述條款後訂購商品，完成付款後即視同同意上述條款。</li>
                </ul>
          </div>


        </div>

        <div class="row">
          <div class="col text-center">
            <h3 class="py-4">推薦商品</h3>

          </div>
        </div>

        <div class="row">

            <div class="col-6 col-md-3 my-2">
              <a href="" class="card-link">
                <div class="card thumbnail-box ">
                  <img src="https://awsimage6.banner.tw/store73/upload/2020Mar/png/20200318111013690_2.png" class="card-img-top">
                  <div class="card-body text-dark">
                    <div class="text-center text-success">
                      <h6 class="text-noDc">Mammut 長毛象</h6>
                    </div>
                                <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                    Aegility Half Zip LS 00710</p>
                    <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
                  </div>
                </div>
              </a>
            </div>

            <div class="col-6 col-md-3 my-2">
              <a href="" class="card-link">
                <div class="card thumbnail-box ">
                  <img src="https://awsimage6.banner.tw/store73/upload/2020Mar/png/20200318111013690_2.png" class="card-img-top">
                  <div class="card-body text-dark">
                    <div class="text-center text-success">
                      <h6 class="text-noDc">Mammut 長毛象</h6>
                    </div>
                                <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                    Aegility Half Zip LS 00710</p>
                    <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
                  </div>
                </div>
              </a>
            </div>

            <div class="col-6 col-md-3 my-2">
              <a href="" class="card-link">
                <div class="card thumbnail-box ">
                  <img src="https://awsimage6.banner.tw/store73/upload/2020Mar/png/20200318111013690_2.png" class="card-img-top">
                  <div class="card-body text-dark">
                    <div class="text-center text-success">
                      <h6 class="text-noDc">Mammut 長毛象</h6>
                    </div>
                                <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                    Aegility Half Zip LS 00710</p>
                    <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
                  </div>
                </div>
              </a>
            </div>

            <div class="col-6 col-md-3 my-2">
              <a href="" class="card-link">
                <div class="card thumbnail-box ">
                  <img src="https://awsimage6.banner.tw/store73/upload/2020Mar/png/20200318111013690_2.png" class="card-img-top">
                  <div class="card-body text-dark">
                    <div class="text-center text-success">
                      <h6 class="text-noDc">Mammut 長毛象</h6>
                    </div>
                                <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                    Aegility Half Zip LS 00710</p>
                    <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
                  </div>
                </div>
              </a>
            </div>
        </div>
       <!-- 內文到此!-->
@stop
