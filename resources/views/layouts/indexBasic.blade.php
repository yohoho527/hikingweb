@extends('layouts.allEmpty')
@section('comment')
<!--輪播圖片!-->
      <div class="row d-flex justify-content-center">
        <div id="carouselExampleIndicators" class="carousel slide  w-100" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
          <div class="carousel-inner">
            <div class="carousel-item active ">
              <img class="d-block w-100 img-fluid" src="{{ asset('images/header.jpg') }}"  alt="First slide">
            </div>
            <div class="carousel-item ">
              <img class="d-block w-100 img-fluid" src="{{  asset('images/header1.jpg')  }}" alt="Second slide">
            </div>
            <div class="carousel-item">
              <img class="d-block w-100 img-fluid" src="{{  asset('images/header2.jpg')  }}" alt="Third slide">
            </div>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
    </div>



    <!--內文!-->
<div class="container pt-2">
  <div class="hot-products my-3 border-right border-left border-bottom">
    <div class="row">
      <div class="col-12 text-center">
        <h1>熱門商品</h1>
      </div>
    </div>
    <div class="row masonry pb-5">

      <div class="col-6 col-md-3  p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
              <img src="https://awsimage6.banner.tw/store73/upload/2020Mar/png/20200318111013690_2.png" class="card-img-top">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>
      </div>
      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://s7d2.scene7.com/is/image/marmot/74830_001_P01?wid=218&hei=327&resMode=sharp&qlt=94" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>
      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://diz36nn4q02zr.cloudfront.net/webapi/imagesV3/Original/SalePage/5872296/1/201329?v=1" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>
      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://s7d2.scene7.com/is/image/marmot/41160_001_P001_S20?wid=218&hei=327&resMode=sharp&qlt=94" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>

      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://diz36nn4q02zr.cloudfront.net/webapi/imagesV3/Cropped/SalePage/6276017/0/141721?v=1" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>
      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://diz36nn4q02zr.cloudfront.net/webapi/imagesV3/Original/SalePage/6272526/0/134800?v=1" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>
      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://s7d2.scene7.com/is/image/marmot/74670_2975_f?wid=218&hei=327&resMode=sharp&qlt=94" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>
      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://diz36nn4q02zr.cloudfront.net/webapi/imagesV3/Cropped/SalePage/5745319/0/151250?v=1" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>


      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://s7d2.scene7.com/is/image/marmot/84140_3432_P01?wid=218&hei=327&resMode=sharp&qlt=94" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>
      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://diz36nn4q02zr.cloudfront.net/webapi/imagesV3/Cropped/SalePage/5745320/0/123120?v=1" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>
      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://cdn-fsly.yottaa.net/59cb961932f01c296feed4f8/www.gregorypacks.com/v~4b.26/dw/image/v2/AAUE_PRD/on/demandware.static/-/Sites-product-catalog/default/dw39437a0d/collections/_gregory/Nano/500x500/1114977406_Nano16_Front.jpg?sw=1500&sfrm=jpg&q=70&yocs=d_h_" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>
      <div class="col-6 col-md-3 p-1 item">
        <a href="" class="card-link">
            <div class="card thumbnail-box ">
               <img src="https://cdn-fsly.yottaa.net/59cb961932f01c296feed4f8/www.gregorypacks.com/v~4b.26/dw/image/v2/AAUE_PRD/on/demandware.static/-/Sites-product-catalog/default/dw687c304a/collections/_gregory/Nano/500x500/GMP_S20_Nano_XS_Waist_MirageBlue_Front34.jpg?sw=1500&sfrm=png&q=70&yocs=d_h_" class="img-fluid">
              <div class="card-body text-dark">
                <div class="text-center text-success">
                  <h6 class="text-noDc">Mammut 長毛象</h6>
                </div>
                            <p>MAMMUT 長袖拉鍊排汗衣 男 龍膽花 </br>
                Aegility Half Zip LS 00710</p>
                <h4 class=" d-inline my-5">$1,600 <h5 class="text-secondary d-inline ml-3"><strike>$2,000</strike><sup class=" text-danger pretty-small">八折優惠</sup></h5>  </h4>
              </div>
            </div>
          </a>

      </div>
    </div>

    <div class="row">
      <div class="col d-flex justify-content-end pr-4 pb-4">
        <a href="" class="btn-outline-dark btn badge-pill">See More>></a>
      </div>
    </div>
  </div>
</div>
@stop
