<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script
    src="https://code.jquery.com/jquery-3.4.1.js"
    integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
    <script src="https://kit.fontawesome.com/7d3fe13f6d.js" crossorigin="anonymous"></script>
    <title>Hello, world!</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('css.css') }}">
    <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
    <script src="{{ asset('js/item.js') }}"></script>
    <script>
    $(function(){
      $('.masonry').masonry({
        itemSelector: '.item',
      })

     });
    </script>
    <script src="{{ asset('js/lightbox.js')}}"></script>
<link href="{{asset('css/lightbox.css')}}" rel="stylesheet" />
  </head>
  <body>
    <div class="container-fluid  ">
      <div class="row">
        <div class="col p-0">
          <nav class="navbar navbar-expand-lg navbar-light  fixed-top box-shadow">
            <a class="navbar-brand d-inline-block text-right " href="#">
              <img src={{  asset('images/SchlagenLOGO_small.png')  }} class="img-fluid " s>
              <span class="h6 d-inline-block " style="color:#0173b9;position: relative;top:20px;line-height: 20px;">
                Outdoor Cloth & Gear<br><small class=""><h5>最好的登山用品</h5></small>
              </span>



            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse  bd-highlight  d-lg-flex justify-content-lg-end" id="navbarTogglerDemo02">
              <ul class="navbar-nav  mt-2 mt-lg-0 " style="position: relative;top:10px;">

                <li class="nav-item  mr-3">




            <form class=" form-inline my-2 my-lg-0 d-block">
              <input class="form-control mr-sm-2 " type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-dark my-2 my-sm-0 " type="submit"><i class="fas fa-search"></i>
                  搜尋</button>
            </form>

                <li class="nav-item active mr-auto">
                  <a class="nav-link mr-3" href="#">首頁 <span class="sr-only">(current)</span></a>
                </li>

                <li class="nav-item dropdown  mr-3">
                  <a class="nav-link dropdown-toggle" href="#" id="storeDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">瀏覽<br class="d-none d-md-inline d-lg-none">商品</a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="#">上衣</a>
                    <a class="dropdown-item" href="#">外套</a>
                    <a class="dropdown-item" href="#">褲子</a>
                    <a class="dropdown-item" href="#">背包/配件</a>
                    <a class="dropdown-item" href="#">鞋子</a>
                  </div>
                </li>
                <li class="nav-item  mr-3">
                  <a class="nav-link" href="/shoppingcart">購物車</a>
                </li>
                <li class="nav-item  mr-3">
                  <a class="nav-link" href="#">訂單<br class="d-none d-md-inline d-lg-none">管理</a>
                </li>




              </ul>

              <button class="btn  my-2 mt-4 btn-success" type="submit" >登入</button>




            </div>
          </nav>
        </div>
      </div>
<!--留白-->
      <div style="height: 90.5px" class="bg-light"></div>


<!--內文!-->
<div class="container pt-2">
    @yield('content')
</div>


 <!--footer!-->
    <div class="container-fluid bg-primary text-center pt-3" id="footer">
      <div class="row w-50 m-auto py-2" >
          <div class="col-6 col-md-3"><a href="" class="text-light">首頁</a></div>
          <div class="col-6 col-md-3"><a href="" class="text-light">瀏覽商品</a></div>
          <div class="col-6 col-md-3"><a href="" class="text-light">購物車</a></div>
          <div class="col-6 col-md-3"><a href="" class="text-light">訂單管理</a></div>

      </div>

      <div class="row w-50 m-auto" >
          <div class="col-12 text-light">
            <p>每日 12:30 ~ 21:20<br>
             07-3308399<br>
             service@Schlagen.com</p>
          </div>


      </div>


      <div class="row w-50 m-auto">
        <div class="col"><p class=" text-right text-light">All images & content ©2004-2020 Schlagen Packs, Inc.</p></div>
      </div>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
